const fs = require("fs");
const puppeteer = require("puppeteer");
const config = require("config");
const axios = require("axios");

const iPhone = puppeteer.devices["iPhone X"];
const useStoreId = config.has("store_id") ? config.get("store_id") : undefined;
const siteDomain = config.get("siteURL").split(/(.*?.com)/g)[1];

const getLocalizationToken = async () => {
  try {
    const { data } = await axios({
      method: "POST",
      url: config.get("localizationURL"),
      data: config.get("localizationAddress"),
      headers: {
        "access-control-allow-origin": siteDomain,
      },
    });

    const storeObj =
      data
        .filter(({ online_status }) => online_status === "online")
        .find(({ store_id }) => !useStoreId || store_id === useStoreId) ||
      data[0];

    return {
      token: storeObj.token,
      storeId: storeObj.store_id,
    };
  } catch (error) {
    console.error(error);
  }
};

const start = async () => {
  try {
    const urlToIntercept = config.get("requestToIntercept");
    const interceptOnlyOn = config.get('interceptOnlyOn');
    const statusOverride = config.get('statusOverride');
    const { token } = await getLocalizationToken();

    const browser = await puppeteer.launch({
      headless: false,
      defaultViewport: null,
      // executablePath: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
      args: [
        "--no-sandbox",
        "--disable-setuid-sandbox",
        "--disable-dev-shm-usage",
        "--ignore-certificate-errors",
        "--start-maximized",
      ],
    });
    const [page] = await browser.pages();

    await page.setRequestInterception(true);

    const mockResponseObject = fs.readFileSync("modifiedData.json", "utf8");
    page.on("request", (request) => {
      if (request.method() === "POST" &&
        request.url() === urlToIntercept &&
        interceptOnlyOn.some(url => request.frame().url().includes(url))) {
        request.respond({
          method: "POST",
          headers: {
            ...request.headers(),
            "access-control-allow-origin": siteDomain,
            "access-control-max-age": "3628800"
          },
          // status: statusOverride || request.response.status(),
          body: mockResponseObject,
        });
      } else request.continue();
    });

    if (config.get("mobileView")) {
      await page.emulate(iPhone);
    }

    const cookies = [
      { name: "disable_gql_ssr", value: "disable", url: siteDomain },
      { name: "localization-token", value: token, url: siteDomain },
      { name: "SEGNUM", value: "1", url: siteDomain },
      { name: "gql_url", value: urlToIntercept, url: siteDomain },
    ];

    await page.setCookie(...cookies);

    const destinationURL = config.get("siteURL");
    await page.goto(destinationURL, {
      waitUntil: "networkidle0",
      referer: 'https://www.google.com/'
    });
  } catch (error) {
    console.log(error);
  }
};

start();
