# Interceptor #

Introduction
------------
HTTP request interceptor.

Quick Start
---------------

**Install dependencies and run application**

```shell
$ npm install
$ npm run start
```
**Edit the default config file if needed or create a local.json config to override default.**
```json
{
	"requestToIntercept": "string", // URL to intercept
	"localizationURL": "string", // URL used to search localization stores
	"siteURL": "string", // URL to open in browser (leda/local)
	"mobileView": boolean, // If true the browser will open with mobile emulator on (Default: iPhone x)
	"store_id": "034131", // Store ID to localize with (Needs to exists based on localization address)
	"localizationAddress": {
		"city": "string",
		"state": "string",
		"limit": integer,
		"occasion_id": "string"
	}
}
```